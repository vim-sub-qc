if exists('b:current_syntax')
	finish
endif

runtime! syntax/ass.vim
unlet b:current_syntax

syntax match subQCSeparator /^#\+$/
syntax match subQCSuggestionSeparator /^>>>$/

highlight default link subQCSeparator Ignore
highlight default link subQCSuggestionSeparator Keyword

let b:current_syntax = 'sub_qc'
