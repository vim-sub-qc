if exists('b:current_syntax')
	finish
endif

syntax region subQCDialogueLine start='^Dialogue:' end='\v([^,]*,){7}' contains=subQCTimeCode,@NoSpell
syntax match subQCTimeCode /\v\d+:\d+:\d+.\d+,\d+:\d+:\d+.\d+/ contained

syntax region subQCASSTag start='{' end='}' display contains=@NoSpell

syntax match subQCASSNewLine /\\N/ contains=@NoSpell

" Treat period groups that aren't "." or "..." as errors
syntax match subQCEllipsisError /\v(\.)@<!\.{2}(\.)@!|\.{4,}/

highlight default link subQCDialogueLine PreProc
highlight default link subQCTimeCode Constant

highlight default link subQCASSNewLine SpecialChar

highlight default link subQCASSTag PreProc

highlight default link subQCEllipsisError Error

let b:current_syntax = 'ass'
