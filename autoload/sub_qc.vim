function! sub_qc#NewEntry(from_pasteboard)
	if a:from_pasteboard
		let dialogue = system('pbpaste')
	else
		let dialogue = getreg('@')[:-2]
	endif

	let current_line = line('.')
	call append(current_line, dialogue)

	" Move to the pasted line
	call cursor(current_line + 1, 1)

	execute "normal! o\<cr>>>>\<cr>\<cr>\<Esc>25i#\<Esc>"

	let dialogue_prefixless = matchlist(dialogue, '\vDialogue:([^,]*,){9}(.*)')[2]
	call append(current_line + 3, dialogue_prefixless)
endfunction
