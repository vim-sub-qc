if exists("b:did_ftplugin")
	finish
endif
let b:did_ftplugin = 1


setlocal nolist
setlocal spell


nnoremap <silent> <buffer> <Plug>SubQCAppendASSEntryToEnd         :call <SID>AppendASSEntryToEnd()<cr>
nnoremap <silent> <buffer> <Plug>SubQCAppendASSEntryToCurrentLine :call <SID>AppendASSEntryToCurrentLine()<cr>
nnoremap <silent> <buffer> <Plug>SubQCInitializeQCFileBoilerplate :call <SID>InitializeQCFileBoilerplate()<cr>


if !hasmapto('<Plug>SubQCAppendASSEntryToEnd') || !maparg('<leader>e', 'n')
	nmap <buffer> <leader>e <Plug>SubQCAppendASSEntryToEnd
endif

if !hasmapto('<Plug>SubQCAppendASSEntryToCurrentLine') || !maparg('<leader>e', 'n')
	nmap <buffer> <leader>w <Plug>SubQCAppendASSEntryToCurrentLine
endif

if !hasmapto('<Plug>SubQCInitializeQCFileBoilerplate') || !maparg('<leader>n', 'n')
	nmap <buffer> <leader>n <Plug>SubQCInitializeQCFileBoilerplate
endif


" Expects two splits, one with the *.ass script and one with the QC document.
" Adds the current line of dialogue as an entry to the QC document at the line
" specified by `a:line_expr`.
function! s:AppendASSEntryToLine(line_expr)
	" Copy current line of dialogue
	silent execute '.y'

	" Switch to the QC document and go to line `a:line_expr`
	wincmd w
	call cursor(a:line_expr, col('.'))

	call sub_qc#NewEntry(0)

	update
endfunction


function! s:AppendASSEntryToEnd()
	call s:AppendASSEntryToLine('$')

	" Switch back to subtitles
	wincmd w
endfunction


function! s:AppendASSEntryToCurrentLine()
	call s:AppendASSEntryToLine('.')
endfunction


" Expects two splits, one with the *.ass script and one with the QC document.
" Initializes the QC report file in the alternate split with the *.ass file
" name and an opening QC entry separator.
function! s:InitializeQCFileBoilerplate()
	let ass_filename = @%

	wincmd w

	call append(0, ass_filename)
	call append(1, '')
	call append(3, '#########################')
	call cursor('$', 0)
endfunction
