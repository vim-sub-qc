if exists("b:did_ftplugin")
	finish
endif
let b:did_ftplugin = 1


setlocal nolist
setlocal spell


nnoremap <silent> <buffer> <Plug>SubQCNewRegister   :call sub_qc#NewEntry(0)<cr>
nnoremap <silent> <buffer> <Plug>SubQCNewPasteboard :call sub_qc#NewEntry(1)<cr>
nnoremap <silent> <buffer> <Plug>SubQCNewLine       :call <SID>NewLineEntry()<cr>

nnoremap <silent> <buffer> [[ :<c-u>call <SID>section_search('a', 'b')<cr>
nnoremap <silent> <buffer> [] :<c-u>call <SID>section_search('b', 'b')<cr>
nnoremap <silent> <buffer> ]] :<c-u>call <SID>section_search('a', '')<cr>
nnoremap <silent> <buffer> ][ :<c-u>call <SID>section_search('b', '')<cr>

nnoremap <silent> <buffer> ]e :<c-u>call <SID>next_separator()<cr>
nnoremap <silent> <buffer> [e :<c-u>call <SID>previous_separator()<cr>
onoremap <silent> <buffer> ]e :<c-u>call <SID>next_separator()<cr>
onoremap <silent> <buffer> [e :<c-u>call <SID>previous_separator()<cr>

nnoremap <silent> <buffer> <Plug>SubQCSaveNewVersion :call <SID>save_new_version()<cr>


if !hasmapto('<Plug>SubQCNewRegister') || !maparg('<leader>e', 'n')
	nmap <buffer> <leader>e <Plug>SubQCNewRegister
endif

if !hasmapto('<Plug>SubQCNewPasteboard') || !maparg('<leader>w', 'n')
	nmap <buffer> <leader>w <Plug>SubQCNewPasteboard
endif

if !hasmapto('<Plug>SubQCNewLine') || !maparg('<leader>l', 'n')
	nmap <buffer> <leader>l <Plug>SubQCNewLine
endif

if !hasmapto('<Plug>SubQCSaveNewVersion') || !maparg('<leader>s', 'n')
	nmap <buffer> <leader>s <Plug>SubQCSaveNewVersion
endif


" Create a new QC entry for a line number
function! s:NewLineEntry()
	execute "normal! oLine \<cr>\<cr>\<Esc>25i#\<Esc>"
	call cursor(line('.') - 2, col('$'))
endfunction


function! s:section_search(direction, flags)
	" ]] moves forward to after the next ####
	" ][ moves forward to before the next ####
	" [[ moves backward to after the previous ####
	" [] moves backward to before the previous ####

	let line = line('.')
	let position = line
	let col = col('.')

	normal! m'

	let i = 0
	let cnt = v:count1
	while i < cnt
		let i = i + 1
		let position = search('\v#{25}', 'W' . a:flags)

		if a:direction ==# 'a'
			" after
			call cursor(position + 1, col)
		elseif a:direction ==# 'b'
			" before
			call cursor(position - 2, col)
		end

		if line ==# line('.')
			let cnt = cnt + 1

			if a:direction ==# 'a'
				call cursor(line - 1, col)
			elseif a:direction ==# 'b'
				call cursor(line + 2, col)
			endif
		endif

		let line = line('.')
	endwhile
endfunction


function! s:find_separator(...)
	let col = col('.')
	let flags = a:0 ==# 1 ? a:1 : ''

	normal! m'

	let i = 0
	while i < v:count1
		let i = i + 1

		let position = search('\v#{25}', 'W' . flags)
		call cursor(position, col)
	endwhile
endfunction


function! s:next_separator()
	call s:find_separator()
endfunction


function! s:previous_separator()
	call s:find_separator('b')
endfunction


function! s:save_new_version()
	let filename = expand('%:t')
	let version_pattern = '-\(\d\+\).qc.txt'
	let matches =  matchlist(filename, version_pattern)

	if !len(matches)
		echohl WarningMsg
		echo 'Filename must include a version in the format "filename-1.qc.txt"'
		echohl None
		return
	endif

	let new_version = matches[1] + 1
	let new_filename = substitute(filename, version_pattern, '-' . new_version . '.qc.txt', '')

	execute 'saveas ' . fnameescape(new_filename)
endfunction
